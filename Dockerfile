# Use the Python3.7
FROM python:3.9

COPY ./app /app

# Set the working directory
WORKDIR /app

# Install the dependencies
RUN pip install -r requirements.txt