#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import sys
from typing import Dict, List

import discord

from __init__ import spike_logger, except_hook
from discord_write.Match import Match
from spike_database.DiscordGuild import DiscordGuild
from spike_database.ResourcesRequest import ResourcesRequest
from spike_requester.CyanideApi import CyanideApi
from spike_requester.Utilities import Utilities as RequesterUtils
from spike_settings.DiscordGuildSettings import DiscordGuildSettings
from spike_settings.SpikeSettings import SpikeSettings
from spike_utilities.Utilities import Utilities

intents = discord.Intents.default()
client = discord.Client(intents=intents)

# Avoid restart the report in case of web socket restart
Already_running = False


def main():
    client.run(SpikeSettings.get_discord_token())


@client.event
async def on_ready():
    global Already_running
    if not Already_running:
        spike_logger.info("match_report connected")
        Already_running = True
        await report()
    else:
        spike_logger.warning("match_report reconnect")


# Main function to manage reporting
async def report():
    excluded_server = Utilities.get_excluded_server()
    common_db = ResourcesRequest()
    nb_match_by_request = SpikeSettings.get_nb_match_by_request()
    nb_league_by_request = SpikeSettings.get_nb_league_by_request()

    platform_list = common_db.get_platforms()

    guild_list = client.guilds
    emo_list = client.emojis

    for platform_id in platform_list:
        # Use set to remove duplicate
        league_list = []
        platform = common_db.get_platform_label(platform_id)
        spike_logger.info(f"Reporting platform: {platform}")

        for guild in guild_list:
            if guild.id not in excluded_server:
                db = DiscordGuild(guild.id)
                try:
                    competitions_id_list = db.get_reported_competitions(platform_id=platform_id)
                    for competition_id in competitions_id_list:
                        competition_data = db.get_competition_data(competition_id)
                        if competition_data is not None:
                            league_name = db.get_league_name(competition_data[7], platform_id)
                            if league_name is not None:
                                league_list.append(league_name)
                            else:
                                spike_logger.info(f"Ignore league: {competition_data[7]} on {platform}")
                        else:
                            spike_logger.info(f"Ignore competition: {competition_id} on {platform}")
                except Exception as e:
                    spike_logger.error(f"Error in match report {e}")
                    spike_logger.error(f"Ignore league: {guild.name} - id: {guild.id}")

        if len(league_list) > 0:
            league_list = list(set(league_list))
            spike_logger.info(f"League to report: {len(league_list)}")
            split_size = nb_league_by_request
            split_list = [league_list[i:i + split_size] for i in range(0, len(league_list), split_size)]

            league_matches = {}
            for current_list in split_list:
                matches_data = CyanideApi.get_matches(current_list, limit=nb_match_by_request, platform=platform, id_only=True)
                if matches_data is not None and type(matches_data) != bool and matches_data.get("matches") is not None:
                    # Create dictionary with league id as key and list of match as value
                    match_list = matches_data.get("matches", [])

                    for match in match_list:
                        if league_matches.get(str(match["idleague"])) is not None:
                            league_matches[str(match["idleague"])].append(match["uuid"])
                        else:
                            league_matches[str(match["idleague"])] = [match["uuid"]]
                else:
                    spike_logger.info(f"No match found for {current_list}")

            for guild in guild_list:
                if guild.id not in excluded_server:
                    try:
                        settings = DiscordGuildSettings(guild.id)
                        matches_to_report = get_new_games(league_matches, guild.id, platform_id)
                        spike_logger.info(f"Guild: {guild.name} - {guild.id} - {platform}")
                        if len(matches_to_report) > 0 and guild.id != 557471602799804436: # ignore message in spike_data
                            compact_report = settings.get_compact_report()
                            display_ip = settings.get_display_impact_player()
                            writer = Match(client, guild.id, emo_list)
                            await writer.write_match_result(matches_to_report, compact_report, display_ip)
                        else:
                            spike_logger.info("No match to report")
                    except sqlite3.OperationalError as e:
                        spike_logger.error(f"Sqlite error: {e}")
                    except sqlite3.InterfaceError as e:
                        spike_logger.error(f"Sqlite error: {e}")
                    except Exception as e:
                        spike_logger.error(e)
        else:
            spike_logger.info(f"No match to report on {platform}")

    spike_logger.info("Report finished")
    sys.exit()


# Check if new game played in each league and get it
def get_new_games(league_matches: Dict[str, List[str]], server_id: int, platform_id: int):
    db = DiscordGuild(server_id)
    league_to_report = db.get_leagues_to_report(platform_id)
    ret_matches = []

    for league_id in league_to_report:
        if str(league_id) in league_matches.keys():
            match_list = league_matches[str(league_id)]
            competition_to_report = db.get_competitions_to_report(league_id, platform_id)
            for match_uuid in match_list:
                if db.is_new_match(match_uuid):
                    match = RequesterUtils.get_match(match_uuid)
                    if match is not None:
                        if match.get_competition_id() in competition_to_report:
                            ret_matches.append(match)
                    else:
                        spike_logger.info("No data found for match {}".format(match_uuid))
                else:
                    spike_logger.debug("Match {} already reported".format(match_uuid))
        else:
            spike_logger.debug("No match in league {} :: guild {}".format(league_id, server_id))
    ret_matches = sorted(ret_matches, key=lambda i: i.get_start_datetime(), reverse=False)
    return ret_matches


sys.excepthook = except_hook

if __name__ == "__main__":
    main()
